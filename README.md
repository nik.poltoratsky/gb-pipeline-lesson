CI pipeline steps:

1. _Build_ - package application to check whether it can be package
2. _Lint_ - lint code to make sure it's correct
3. _Test_ - execute tests to make sure everything works correctly
