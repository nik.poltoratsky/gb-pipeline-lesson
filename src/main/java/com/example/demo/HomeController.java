package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

  /**
   * greets.
   * @return Hello, World!
   */
  @RequestMapping("/")
  public @ResponseBody String greeting() {
    return "Hello, World!";
  }
}
